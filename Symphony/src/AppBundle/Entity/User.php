<?php

namespace AppBundle\Entity;

use Serializable;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User implements UserInterface, Serializable, EquatableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;
	
	/**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, unique=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido1", type="string", length=255)
     */
    private $apellido1;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido2", type="string", length=255)
     */
    private $apellido2;
	
	/**
     * @var array
     *
     * @ORM\Column(name="role", type="string", length=255)
     */
    private $role;

    private $misExamenes;
    private $misMatriculas;


    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
	
	/**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password (UserInterface)
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
	
	/**
     * Set role
     *
     * @param string $role
     *
     * @return User
     */
    public function setRole($rol)
    {
        $this->role = $role;

        return $this;
    }
	
	/**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return User
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido1
     *
     * @param string $apellido1
     *
     * @return User
     */
    public function setApellido1($apellido1)
    {
        $this->apellido1 = $apellido1;

        return $this;
    }

    /**
     * Get apellido1
     *
     * @return string
     */
    public function getApellido1()
    {
        return $this->apellido1;
    }

    /**
     * Set apellido2
     *
     * @param string $apellido2
     *
     * @return User
     */
    public function setApellido2($apellido2)
    {
        $this->apellido2 = $apellido2;

        return $this;
    }

    /**
     * Get apellido2
     *
     * @return string
     */
    public function getApellido2()
    {
        return $this->apellido2;
    }

    public function setMisExamenes($misExamenes)
    {
        $this->misExamenes = $misExamenes;

        return $this;
    }

    public function getMisExamenes()
    {
        return $this->misExamenes;
    }

    public function setMisMatriculas($misMatriculas)
    {
        $this->misMatriculas = $misMatriculas;

        return $this;
    }

    public function getMisMatriculas()
    {
        return $this->misMatriculas;
    }
	
		// UserInterface
		public function getRoles(){
			return array($this->getRole());
		}
		
		public function getSalt(){
			return null;
		}
		
		public function getUsername(){
			return $this->getNombre().' '.$this->getApellido1();
			//return $this->email;
		}
		
		public function eraseCredentials(){
			//TODO:
		}
		
		// Serializable
		public function serialize(){
			return serialize(array(
				$this->id,
				$this->email,
				$this->password
			));
		}
		
		public function unserialize($serialized){
			list(
				$this->id,
				$this->email,
				$this->password
			) = unserialize($serialized);
		}
		
		//EquatableInterface
		public function isEqualTo(UserInterface $user){
			return $this->id === $user->getId();
		}
}

