<?php namespace AppBundle\Model;

require_once("Materia.php");

class Examen { 

	// Atributos
	private  $_nota;
	private $_materia;
	
	// Getters y Setters
	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}
	
	public function __set($property, $value) {
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
		
		return $this;
	}
	
	// Construccion
	public function __construct(Materia $pMateria) {
		$this->_materia = $pMateria;
	}
	
	// Métodos de interface
	public function evaluaExamen(float $pNota) {
		$this->_nota = $pNota;
		return $this;
	}
	
	public function esDeMateria(Materia $pMateria):bool{
		return $this->_materia === $pMateria;
	}
}