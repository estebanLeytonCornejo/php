<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login_route")
     */
    public function loginAction()
    {
		$autenticacion = $this->get('security.authentication_utils');
		$error = $autenticacion->getLastAuthenticationError();
        return $this->render('Security/login.html.twig', array(
            'error' => $error
        ));
    }

    /**
     * @Route("/loginCheck", name="login_check")
     */
    public function loginCheckAction(){
		//$this->redirectToRoute("homepage");
    }
	
	/**
     * @Route("/loginAfter", name="after_login_route_name")
     */
    public function loginAferAction(){
		$user = $this->getUser();
		if($user->getRole()==='ROLE_PROF'){
			return $this->redirectToRoute("menu_profesor");
		}
		else if($user->getRole()==='ROLE_ALU'){
			return $this->redirectToRoute("menu_alumno");
		}
		else if($user->getRole()==='ROLE_ADMIN'){
			return $this->redirectToRoute("app_admin_index");
		}
		else{
			throw $this->createAccessDeniedException();
		}
	}

}
