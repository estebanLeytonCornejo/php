<?php session_start();
// Desactivar toda notificación de error
error_reporting(0);

function debug($var){
    echo '<pre>'.print_r($var, true).'</pre>';
}
?>
<style>
    body{
        font-family: verdana;
    }
    #panelBotones{
        width: 900px;
        height: 60px;
        border: 3px solid gray;
        margin-right: auto;
        margin-left: auto;
    }
    #floatL{
        float: left;
        margin-left: 50px;
    }
    #floatR{
        float: right;
        margin: 20px 40px 0 0;
    }
    #formatoLog{
        width: 500px;
        height: 200px;
        margin-left: auto;
        margin-right: auto;
        background-color: gray;
    }
    #entrada{
        width: auto;
        height: 100px;
        background-color: rgb(245, 224, 171);
        margin-bottom: 10px;
    }
    #formatoLog form{
        float: left;
        margin: 20px 0 0 150px;
    }
    #formatoLog input{
        display: block;
    }
    #mostrarAlumnos{
        width: 800px;
        height: 500px;
        margin-left: auto;
        margin-right: auto;
        border: 3px solid gray;
        background-color: darkgray;
        text-align: center;
    }
    #nombres{
        width: 400px;
        height: 400px;
        float: left;
        margin-left: 190px;
        border: 1px solid black;
        font-size: 1.5em;
        color: white;
    }
    #formNuevoAlumno ,#opcionesEx{
        width: 500px;
        height: 200px;
        margin-left: auto;
        margin-right: auto;
        background-color: gray;
    }
    #formNuevoAlumno form{
        float: left;
        margin: 20px 0 0 150px;
    }

    #formNuevoAlumno input{
        display: block;
    }

    #opcionesEx input{
        width: 200px;
        margin: 20px 0 0 150px;
    }
    #cabecera{
        width: 100%;
        padding: 4%;
        background-color: red;
    }
    #cabecera input{
        float:left;
        margin-right: 30px;;
    }
    .boton{
        background-color: #1b1b1b;
        border: 2px solid #1b1b1b;
        color: #ffffff;
        padding: 5px 10px;
        border-radius: 5px;
        cursor:pointer;
        text-decoration:none;
        font-family: arial;
    }
    table{
        background-color: lightgrey;
        width: 100%;
        text-align: center;
    }
    table td{
        background-color: white;
    }
    table th{
        background-color: darkgray;
    }
    #mediaTotalEx{
        background-color: cornflowerblue;
    }
</style>
<html>
    <body>
        <?php
        //FUNCIÓN QUE INICIA AL FORM DE LOGIN
        function	Login(){
            echo ' <div id="formatoLog">
                      <FORM ACTION = "index.php" METHOD = "GET">
				         User 8082 <INPUT TYPE = "text" NAME = "user"> <br>
				         Pass <INPUT TYPE = "text" NAME = "pass"> <br>
				             <INPUT TYPE = "hidden" NAME = "action" VALUE = "login"> <br>
				              <INPUT TYPE = "submit" VALUE = "Login">
				      </FORM></div> ';
        }
        ///FUNCIÓN QUE MUESTRA LA INTRO DE LA APLICACIÓN
        function mostrarIntro(){
            echo '<div id="cabecera">
            <form action="index.php" method="get">
                <div><input class="boton" type= "submit" name= "action" value="Mostrar alumnos"></div>
                <div><input class="boton" type= "submit" name= "action" value="Crear alumnos"></div>
                <div><input class="boton" type= "submit" name= "action" value="Simulación examenes"></div>
                <div><input class="boton" type= "submit" name= "action" value="Resultado examenes"></div>
                <div><input class="boton" type= "submit" name= "action" value="Cerrar sesión"></div>
            </form>
        </div>';

        }
        //FUNCION QUE MUESTRA LOS ALUMNOS EXISTENTES
        /*
        function mostrarAlumnos() {
            $alumnos=$_SESSION['alumnos'];
            echo  '<div id="mostrarAlumnos">
                   <h1>Listado de alumnos</h1>
                   <div id="nombres">';foreach ($alumnos as $al ) {
                       echo array_values($al)[0]." ". array_values($al)[1]."<br>";}'</div>
                 </div>';
        }
        */
        //FUNCIÓN QUE TIENE EL FORM QUE RECIBE DATOS DEL NUEVO ALUMNO
        function nuevoAlumno(){
            echo '<div id="formNuevoAlumno">
            <FORM ACTION = "index.php" METHOD = "GET">
					Introduce el nombre: <INPUT TYPE = "text" NAME = "nombre"> <br>
					Introduce el apellido: <INPUT TYPE = "text" NAME = "apellido"> <br>
					<INPUT TYPE = "hidden" NAME = "action" VALUE = "nuevoAlumno"> <br>
					<INPUT TYPE = "submit" VALUE = "Enviar">
				</ FORM></div> ';
        }
        /*
        function mostrarNotasAlumnos() {
            $alumnos=$_SESSION['alumnos'];
            echo  '<div id="mostrarAlumnos">
                   <h1>Listado de alumnos</h1>
                   <div id="nombres">';foreach ($alumnos as $al ) {
                       echo array_values($al)[0]." ". array_values($al)[1]."NOTAS : ".mostrarNotas($al['notas'])."<br>";}'</div>
                 </div>';
        }
        */
        //MUESTRA LAS NOTAS DEL ALUMNO
        function mostrarNotas($array){
            $notas ="";
            $alumnos=$_SESSION['alumnos'];
            foreach ($array as $mostrar) {
                $notas = $notas ."," .$mostrar;
            }
            return $notas;
        }
        function mostrarAlumnos() {
            $alumnos=$_SESSION['alumnos'];

            echo "<div class='Encabezado'>";
            echo "<h2>Lista de Alumnos </h2>";
            echo "</div>";

            echo "<table>";
            echo "<tr>";
            echo "<th>Nombre</th>";
            echo "<th>Apellido</th>";
            echo "</tr>";
            foreach ( $alumnos as $al ) {
                echo "<tr>";
                echo "<td>",$al['nombre'],"</td>";
                echo "<td>",$al['apellido'],"</td>";
                echo "</tr>";
            }
            echo "</table>";
            echo "<br>";
            echo "El numero total de alumnos es: " . count($alumnos);
        }
        function redondeo($num){
            $resultado += $num;
            $redondeo = round($resultado,2,PHP_ROUND_HALF_DOWN); 
            return $redondeo ;
        }

        function mostrarMediaGenerales($num){
            $_SESSION['suma']+=$num;
            $_SESSION['contador']++; 
            return $_SESSION['suma']/$_SESSION['contador'] ;
        }
        $arrayCont0 = [];
        $arrayCont1 = [];
        $arrayCont2 = [];

        function notas($array){
            foreach($array as $notas){
                return $notas;
            }

        }

        function vueltasNotas(){
            $alumnos=$_SESSION['alumnos'];           
            $cuenta;           
            foreach ($alumnos as $al) {             
                foreach ($al as $key ){
                    foreach ($key as  $value){
                        $cuenta=  count($key) ;                                               
                    }
                }
            }
            return $cuenta;
        }

        function imprimeListaAlumnosNotas(){
            $contador = 0;

            echo '</form>';          
            echo "<h2>Lista de Alumnos y notas </h2>";
            echo "</div>";
            echo "<table>";
            echo "<tr>";         
            echo      "<th>Nombre</th>";
            echo      "<th>Apellido</th>";
            for ($i=0; $i <vueltasNotas() ; $i++) { 
                echo  "<th>".($i+1)."º Nota</th>";
            }
            echo "<th>Media</th>";
            echo   "</tr>";

            foreach($_SESSION['alumnos'] as $al){
                $numeroExames = count($al['notas']);
                if($numeroExames == 0){
                    echo "";
                }else{
                    echo '<tr>';
                    echo '<td>'.$al['nombre'].'</td>';
                    echo " ";
                    echo '<td>'.$al['apellido'].'</td>';  
                    for($i = 0; $i < $numeroExames; $i++){
                        echo '<td>'.array_values($al['notas'])[$i].'</td>';
                    }
                    echo " ";
                    echo '<td><STRONG>'.round((array_sum($al['notas'])/$numeroExames),2).'</STRONG></td>';
                    echo '</tr>';   
                }
            }
            echo "</table>";

        }



        //FUNCION QUE PROCESA LOS DATOS PROVENIENTES DEL FORM Y/0 FUNCION nuevoAlumno()
        function procesoNuevoAlumno() {
            if (isset($_GET['action']) && $_GET['action'] == "nuevoAlumno") {
                if ($_GET['nombre'] != NULL) {
                    $_SESSION['alumnos'][] = array('nombre' => $_GET['nombre'],'apellido' => $_GET['apellido'],'notas' => []);

                }
            }
        }

        //FUNCION MUESTRA BOTONES PARA AÑADIR NOTAS RANDOM AL ARRAY
        function mostrarOpcionesExamen(){
            //si borro el action funciona solo el último if else
            echo '<div id="opcionesEx">
            <FORM  METHOD = "GET">
                    <input type="hidden" name="action" value="'.$_GET['action'].'">
					<INPUT TYPE = "submit" NAME = "dificultad" value="facil"> <br>
                    <INPUT TYPE = "submit" NAME = "dificultad" value="medio"> <br>
                    <INPUT TYPE = "submit" NAME = "dificultad" value="dificil"> <br>
				</ FORM></div> ';

            if(isset($_GET['dificultad'])){
                switch($_GET['dificultad']){
                    case 'facil':
                        insertarNotaBaja(rand());
                        break;
                    case 'medio':
                        insertarNotaMedia(rand());
                        break;
                    case 'dificil':
                        $nota1 = rand();
                        $nota2 = rand();
                        insertarNotaAlta($nota1,$nota2);
                        break;
                }
            }

        }

        //FUNCION INSERTA NOTAS RANDOM EN ARRAY
        function insertarNotaBaja($nota){

            for ($i = 0; $i < count($_SESSION['alumnos']); $i++) {
                $nota=rand(1*100,5*100)/100;
                array_push($_SESSION['alumnos'][$i]['notas'],$nota);
            }
        }
        function insertarNotaMedia($nota){
            for ($i = 0; $i < count($_SESSION['alumnos']); $i++) {
                $nota=rand(5*100,10*100)/100;
                array_push($_SESSION['alumnos'][$i]['notas'],$nota);
            }
        }
        function insertarNotaAlta($nota1,$nota2){
            if($nota1<$nota2){
                for ($i = 0; $i < count($_SESSION['alumnos']); $i++){
                    $nota1=rand(1*100,10*100)/100;
                    array_push($_SESSION['alumnos'][$i]['notas'],$nota1);
                }
            }else if($nota2<$nota1){
                for ($i = 0; $i < count($_SESSION['alumnos']); $i++) {
                    $nota2=rand(1*100,10*100)/100;
                    array_push($_SESSION['alumnos'][$i]['notas'],$nota2);
                }
            }
        }
    

        ///CODIGO DE LA SESION
        if (!existenDatosEnSession()) {
            cargaArray();
        }

        function existenDatosEnSession () {
            return isset ($_SESSION['alumnos']) && is_array ($_SESSION['alumnos']);
        }

        function cerrarSesion(){
            echo '<div style="color:grey; border:solid 1px; padding: 10px;">Cerrando sesión</div>';
            header('Refresh:2 url=index.php');
            session_destroy();
        }



        //SE CARGAN LOS DATOS EN EL ARRAY
        function cargaArray(){

            $alumnos = array(
                array('nombre' => 'Eugenio','apellido'=> 'Martínez',
                      'edad' => 45,
                      'notas' => array()),
                array('nombre' => 'Marta','apellido'=>'Martínez',
                      'edad' => 22,
                      'notas' => array()),
                array('nombre' => 'Nacho','apellido'=>'Herrera',
                      'edad' => 25,
                      'notas' => array()),
                array('nombre' => 'Anxo', 'apellido'=>'Iglesias',
                      'edad' => 32,
                      'notas' => array()),
                array('nombre' => 'Valentina','apellido'=>'Iglesias',
                      'edad' => 30,
                      'notas' => array())
            );
            $_SESSION['alumnos'] =$alumnos;
        }

        $user ="user";
        $pass= "123";
        //SE CREA SWITCH

        if(isset($_GET['user']) && isset($_GET['pass'])){ //COMPRUEBA SI USER Y PASS ESTEN DEFINIDOS
            //ES DECIR,LAS VARIABLES $USER Y $PASS TIENEN DATOS(TRUE).SI NO LOS TIENEN SE PRODUCE FALSE Y VA AL ELSE.
            if($_GET['user']==$user && $_GET['pass']==$pass){//COMPRUEBA QUE LOS DATOS INTRODUCIDOS COINCIDAN
                //login ok
                $_SESSION['login'] = true;//TRUE EQUIVALE A UN OK.DENTRO DE $_SESSION['login'] ESTÁN LOS DATOS DEL USER Y DEL PASS.
                print_r( $_SESSION['login'] );
                echo '<div style="color:green; border:solid 1px; padding: 10px;">Login válido</div>';
                header('Refresh:2; url=index.php');
            }else{
                //login no
                echo '<div style="color:red; border:solid 1px; padding: 10px;">No has introducido los datos correctos</div>';
                header('Refresh:2; url=index.php');
            }
        }else{
            if(isset($_SESSION['login']) && $_SESSION['login'] == true){//SI LA SESION ESTÁ DEFINIDA Y ES == A TRUE SE CARGA EL SWITCH
                //está iniciada la sesión
                mostrarIntro();//MUESTRA LOS BOTONES
                if (isset($_GET['action'])){
                    switch($_GET['action']){
                        case 'Mostrar alumnos':
                            mostrarAlumnos();
                            break;
                        case 'Crear alumnos':
                            nuevoAlumno();
                            break;
                        case 'Simulación examenes':
                            mostrarOpcionesExamen();
                            break;
                        case 'Resultado examenes':
                            //mostrarNotasAlumnos();
                            imprimeListaAlumnosNotas();

                            break;

                        case 'Cerrar sesión':
                            session_destroy();
                            echo '<div style="color:grey; border:solid 1px; padding: 10px;">Cerrando sesión</div>';
                            header('Refresh:2 url=index.php');
                            break;

                    }
                }


            }else{
                //no está iniciada la sesión
                Login();
            }
        }
        procesoNuevoAlumno();

        //echo '$_GET';
        //debug($_GET);
        //echo '$_SESSION';
        //debug($_SESSION);
        //echo $_GET['action'];

        //session_destroy();

        ?>
    </body>

</html>
