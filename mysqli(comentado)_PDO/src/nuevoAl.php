<?php


include("Alumno.php");
include("nuevaEntrada.php");




class nuevoAl implements Alumno{
    private $id;
    private $first_name;
    private $last_name;
    private $Email;
    private $Birthdate;
    private $DischargeDate;

    public function __construct($id,$first_name,$last_name,$Email,$Birthdate,$DischargeDate){
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->Email = $Email;
        $this->Birthdate = $Birthdate;
        $this->DischargeDate = $DischargeDate;
        $this->id = $id;

    }

    public function getId():int{		
        return $this->id;
    }

    public function getFirstName():string{
        return $this->first_name;
    }

    public function getLastName():string{
        return $this->last_name;
    }

    public function getEmail(){
        return $this->Email;
    }

    /** Actualización del email del alumno. Debe ser válido */
    public function setEmail(string $pEmail){
        $this->Email = $Email;
    }

    public function getBirthdate(){
        return $this->Birthdate;
    }

    public function getDischargeDate():DateTime{
        return $this->DischargeDate;
    }

    public static function get_alumno_by_id($pId){
        $var = intval($pId);
        //$mysqli = new mysqli("127.0.0.1","dwcs","abc123.","dwcs_mysqli_dbo");
        $conn = new PDO('mysql:host=127.0.0.1;dbname=dwcs_mysqli_dbo', "dwcs", "abc123.");
        $consulta = "SELECT * FROM authors where id=$var";
        $resultado = $conn->query($consulta);
        $persona = $resultado->fetch(PDO::FETCH_OBJ);	
        $alumno = new nuevoAl($persona->id,$persona->first_name,$persona->last_name,$persona->email,$persona->birthdate,$persona->added);

        return $alumno;		
    }
    /*
		$consulta = "SELECT * FROM authors where id=$var";
		$resultado = $mysqli->query($consulta);	
		//if ($resultado = $mysqli->query($consulta)){
			//while ($persona = $resultado->fetch_object()){
			$persona = $resultado->fetch_object();
			$alumno = new nuevoAl($persona->id,$persona->first_name,$persona->last_name,$persona->email,$persona->birthdate,$persona->added);
				return $alumno;
			//}	
	//}


*/
    public static function get_alumno_by_email(string $pEmail){	
        //$mysqli = new mysqli("127.0.0.1","dwcs","abc123.","dwcs_mysqli_dbo");
        $conn = new PDO('mysql:host=127.0.0.1;dbname=dwcs_mysqli_dbo', "dwcs", "abc123.");

        $consulta = "SELECT * FROM authors where email=$pEmail";
        $resultado = $conn->query($consulta);			
        $persona = $resultado->etch(PDO::FETCH_OBJ);
        $alumno = new nuevoAl($persona->id,$persona->first_name,$persona->last_name,$persona->email,$persona->birthdate,$persona->added);
        return $alumno;		
    }

    /** @return boolval ¿Tiene entradas el alumno en el blog */


    public function tengo_entradas_en_el_blog():bool{
        return true;
    } 

    /** @return boolval ¿Eres mayor de edad */
    public function soy_mayor_de_edad():bool{

        $edadMayor = 18;
        $fechaNac = DateTime::createFromFormat('Y-m-d',$this->getBirthdate());
        $calc = $fechaNac->diff(new DateTime());
        $edad = $calc->y;
        if($edad < $edadMayor){
            return false;
        }else{
            return true;
        }
     
    }

    /** @return array<Entrada> Retorna un array con todas las entradas coincidentes (mediante patrón) con títulos de entradas del alumno en el Blog de clase */
    public function all_mis_entradas_en_el_blog():array{
        $entradas = [];
        $idAlumno = intval($this->getId());
        $recogerIdAlumno = $this->get_alumno_by_id($idAlumno);
        $conn = new PDO('mysql:host=127.0.0.1;dbname=dwcs_mysqli_dbo', "dwcs", "abc123.");
        //$mysqli = new mysqli("127.0.0.1","dwcs","abc123.","dwcs_mysqli_dbo");
        $consulta = "SELECT * FROM posts where author_id=$idAlumno";
        if($resultado = $conn->query($consulta)){
            while($linea = $resultado->fetch(PDO::FETCH_OBJ)){
                $id = intval($linea->author_id);
                $alumno = $this->get_alumno_by_id($id);	
                $nuevaEnt = new nuevaEntrada($linea->id,$alumno,$linea->title,$linea->content,$linea->description,$linea->date);
                array_push($entradas, $nuevaEnt);
            }
        }

        return $entradas ;

    }



    public function all_mis_entradas_en_el_blog_tituladas(string $pPattern):array{

        $entradas = [];
        $id = intval($this->getId());
        $alumno = $this->get_alumno_by_id($id);		
        //$mysqli = new mysqli("127.0.0.1","dwcs","abc123.","dwcs_mysqli_dbo");
        $conn = new PDO('mysql:host=127.0.0.1;dbname=dwcs_mysqli_dbo', "dwcs", "abc123.");
        $consulta = "SELECT * from posts where author_id =$id and title REGEXP '$pPattern'";
        if($resultado = $conn->query($consulta)){
            while($linea = $resultado->fetch(PDO::FETCH_OBJ)){	
                $nuevaEnt = new nuevaEntrada($linea->id,$alumno,$linea->title,$linea->content,$linea->description,$linea->date);
                array_push($entradas, $nuevaEnt);
            }
        }

        return $entradas ;
    }

    /** @return array<Entrada> Retorna un array con todas las entradas coincidentes (mediante patrón) con títulos o contenido de entradas del alumno en el Blog de clase */
    public function all_mis_entradas_en_el_blog_contienen(string $pPattern){
        $entradas = [];
        $id = intval($this->getId());
        $alumno = $this->get_alumno_by_id($id);
        $conn = new PDO('mysql:host=127.0.0.1;dbname=dwcs_mysqli_dbo', "dwcs", "abc123.");		
        //$mysqli = new mysqli("127.0.0.1","dwcs","abc123.","dwcs_mysqli_dbo");

        $consulta = "SELECT * from posts where author_id = $id and content REGEXP  '$pPattern'";
        if($resultado = $conn->query($consulta)){
            while($linea = $resultado->fetch(PDO::FETCH_OBJ)){			
                $nuevaEnt = new nuevaEntrada($linea->id,$alumno,$linea->title,$linea->content,$linea->description,$linea->date);
                array_push($entradas, $nuevaEnt);
            }
        }

        return $entradas ;
    }




    /** Borra todas las entradas coincidentes (mediante patrón) con títulos de entradas del alumno en el Blog de clase
	* @return integer Número de entradas eliminadas */
    public function remove_mis_entradas_en_el_blog_tituladas(string $pPattern){
        $borradas = intval(0);
        $id = intval($this->getId());
        $alumno = $this->get_alumno_by_id($id);
        $conn = new PDO('mysql:host=127.0.0.1;dbname=dwcs_mysqli_dbo', "dwcs", "abc123.");			
        //$mysqli = new mysqli("127.0.0.1","dwcs","abc123.","dwcs_mysqli_dbo");
        $consulta = "DELETE  from posts where author_id = $id and title REGEXP '$pPattern'";
        $conn->query($consulta);

        /*
		if($resultado = $mysqli->query($consulta)){
			while($linea = $resultado->fetch_object()){			
			$nuevaEnt = new nuevaEntrada($linea->id,$alumno,$linea->title,$linea->content,$linea->description,$linea->date);
			array_push($entradas, $nuevaEnt);
			$borradas++;
			}
		}

		return $borradas ;

		*/
    }



    /** Borra todas las entradas coincidentes (mediante patrón) con títulos o contenido  de entradas del alumno en el Blog de clase
	* @return integer Número de entradas eliminadas */
    public function remove_mis_entradas_en_el_blog_contienen(string $pPattern){
        $id = intval($this->getId());
        $alumno = $this->get_alumno_by_id($id);		
        //$mysqli = new mysqli("127.0.0.1","dwcs","abc123.","dwcs_mysqli_dbo");
        $conn = new PDO('mysql:host=127.0.0.1;dbname=dwcs_mysqli_dbo', "dwcs", "abc123.");	
        $consulta = "DELETE  from posts where author_id = $id and content REGEXP '$pPattern'";
        $conn->query($consulta);
    }



    /** Introduce una nueva entrada en el Blog de clase del alumno */
    public function nueva_entrada_en_blog(string $pTitulo, string $pDescripcion, string $pContenido){
        $id = intval($this->getId());
        $alumno = $this->get_alumno_by_id($id);
        $fecha = date("Y-m-d");
        //$mysqli = new mysqli("127.0.0.1","dwcs","abc123.","dwcs_mysqli_dbo");
        $conn = new PDO('mysql:host=127.0.0.1;dbname=dwcs_mysqli_dbo', "dwcs", "abc123.");	
        $consulta = "INSERT into posts (author_id,title,description,content,date) values('$id','$pTitulo','$pDescripcion','$pContenido','$fecha')";

        $conn->query($consulta);
        /*
		$nuevaEntrada = new nuevaEntrada($id,$alumno,$pTitulo,$pDescripcion,$pContenido,$fecha);
		return $nuevaEntrada;
		*/

    }
}

?>