<?php namespace AppBundle\Model;

require_once("ExamenTeorico.php");
require_once("ExamenPractico.php");
require_once("Oficina.php");

class Profesor {
	//Atributos
	private $_materiasImpartidas = array();
	
	private $_codigo;
	private $_oficina;

	//Getters y Setters
	public function getCodigo(){
		return $this->_codigo;
	}
	
	private function getOficina(){
		if(!$this->_oficina)
			return $this->_oficina;
		else throw new RuntimeException("Código que no debiera ser alcanzable. Inyección de una instancia nula");
	}
	
	public function setOficina(Oficina $pOficina){
		if($pOficina)
			$this->_oficina = $pOficina;
		else throw new RuntimeException("Código que no debiera ser alcanzable. Inyección de una instancia nula");
	}

	//Construcción
	public function __construct(string $pCod) {
		$this->_codigo = $pCod;
	}
	
	//Métodos de interface
	public function dameTusAlummos() {
		$misAlumnos = array();
		// Consulto todas mis Materias para consultar los alumnos matriculados en ellas
		foreach ($this->_materiasImpartidas as $unaMateria) {
			$alumnosEnMateria = $this->dameTusAlumnosDeMateria($unaMateria);
			// Doc: (https://www.php.net/manual/es/function.array-merge.php)
			// Las claves del primer array se preservaran. Si una clave existe en ambos arrays, se usará el elemento del primer array, y el elemento de la clave coincidente del segundo array será ignorado.
			$misAlumnos =  $misAlumnos + $alumnosEnMateria;
		}
		return $misAlumnos;
	}
	
	public function dameTusAlumnosDeMateria(Materia $pMateria){
		$misAlumnos = $this->getOficina()->getAlumnosFromMateria($pMateria);
		return $misAlumnos;
	}
	
	public function dameTusMaterias() {
		return $this->_materiasImpartidas;
	}
	
	public function asignateMaterias(Array $pMaterias) {
		foreach ($pMaterias as $unaMateria) {
			array_push($this->_materiasImpartidas, $unaMateria);
		}
	}
	
	public function impartesMateria(Materia $pMateria):bool{
		foreach ($this->_materiasImpartidas as $unaMateria) {
			if($unaMateria == $pMateria)
			return true;
		}
		return false;
	}
	
	public function esPosibleLaAsignacionDeNuevasMaterias(array $pCatalogoDeMaterias):bool{
		//Será posible la asignación (de momento) sólo si ya no está asociado a $pCatalogoDeMaterias
		foreach ($pCatalogoDeMaterias as $pUnaMateria) {
			if(!$this->impartesMateria($pUnaMateria)){
				return true;
			}
		}
		return false;
	}

	public function simulaExamenTeorico() {
		foreach ($this->_materiasImpartidas as $unaMateria) {
			foreach ($this->dameTusAlumnosDeMateria($unaMateria) as $unAlumno) {
				$this->simulaExamenParaAlumno($unAlumno, new ExamenTeorico($unaMateria), 0, 10);
			}
		}
		syslog(LOG_DEBUG, "Las notas ya han sido asignadas a los alumnmos");
	}

	public function simulaExamenPractico() {
		foreach ($this->_materiasImpartidas as $unaMateria) {
			foreach ($this->dameTusAlumnosDeMateria($unaMateria) as $unAlumno) {
				$this->simulaExamenParaAlumno($unAlumno, new ExamenPractico($unaMateria), 0, 10);
			}
		}
		syslog(LOG_DEBUG, "Las notas ya han sido asignadas a los alumnmos");
	}

	//Métodos privados
	private function simulaExamenParaAlumno(Alumno $pAlumno, Examen $pExamen, int $pNotaMin, int $pNotaMax) {
		$valorEntero = rand($pNotaMin, $pNotaMax);
		$pAlumno->entregaExamen($pExamen->evaluaExamen($valorEntero));
	}
}