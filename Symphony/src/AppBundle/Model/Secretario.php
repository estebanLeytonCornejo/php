<?php namespace AppBundle\Model;

require_once("Alumno.php");
require_once("Oficina.php");

class Secretario implements Oficina{
	//Atributos
	private $_alumnos = array();
	private $_profesores = array();
	private $_materias = array();
	
	// Getters y Setters
	private function getAlumnos() {
		return $this->_alumnos;
	}
	
	private function getProfesores() {
		return $this->_profesores;
	}
	
	// Construcción
	
	
	// Métodos de interface
	public function getAlumnoFromDni(string $pDni): ?Alumno {
		$a = $this->getAlumnos();
		foreach($a as $unAlumno){
			if($unAlumno->_dni === $pDni){
				return $unAlumno;
			}
		}
		//https://www.php.net/manual/en/migration71.new-features.php
		return null;
	}
	
	public function getProfesorFromCodigo(string $pCod): ?Profesor {
		$a = $this->getProfesores();
		foreach($a as $unProfesor){
			if($unProfesor->getCodigo() === $pCod){
				return $unProfesor;
			}
		}
		//https://www.php.net/manual/en/migration71.new-features.php
		return null;
	}

	public function createAlumno(String $pDni, String $pNombre, String $pApellido1, String $pApellido2) {
		$nuevoAlumno = new Alumno($pDni, $pNombre, $pApellido1, $pApellido2);
		$_alumnos[$pDni] = $nuevoAlumno;
		return $nuevoAlumno;
	}

	public function addMateriaMatriculable(Materia $pMateria) {
		$this->_materias[$pMateria->_codigo] = $pMateria;
	}

	public function getMateriasMatriculables() {
		return $this->_materias;
	}
	
	public function getMateriaPorCodigo($pCod){
		if(isset($this->_materias[$pCod])){
			return $this->_materias[$pCod];
		}
		else return null;
	}

	public function asignaAlumnos(array $pAlumnos) {
		foreach ($pAlumnos as $unAlumno) {
			$this->_alumnos[$unAlumno->_dni] = $unAlumno;
		}
	}
	
	public function asignacionProfesores(array $pProfesores) {
		foreach ($pProfesores as $unProfesor) {
			//"Mecanismo" para que el profesor pueda pedir datos sobre el centro (inyección de dependencia)
			$unProfesor->setOficina($this);
			$this->_profesores[$unProfesor->getCodigo()] = $unProfesor;
		}
	}
	
	public function getAlumnosFromMateria(Materia $pMateria):?array{
		$alumnosEnMateria = array();
		foreach($this->_alumnos as $unAlumno){
			if($unAlumno->estasMatriculadoEn($pMateria)){
				$alumnosEnMateria[$unAlumno->_dni] = $unAlumno;
			}
		}
		return $alumnosEnMateria;
	}

	// Métodos privados
}