<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AlumnoController extends Controller
{
    /**
     * @Route("/alumno", name="menu_alumno")
	 * @Security("has_role('ROLE_ALU')")
     */
    public function indexAction(){
        return $this->render('Alumno/menu.html.twig');
       
    }

     /**
     * @Route("/index.php")
     */
    public function loginCheckAction()
    {
     return $this->redirectToRoute("homepage");
    }

}
