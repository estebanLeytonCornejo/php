<?php

namespace AppBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Profesor;

class ProfesorController extends Controller
{
	/**
     * @Route("/profesor", name="menu_profesor")
	 * @Security("has_role('ROLE_PROF')")
     */
    public function indexAction()
    {
        
		$user = $this->getUser();
        return $this->render('Profesor/menu.html.twig', array(
            "username" => $user->getUsername()  

        ));
       
    }
    /**
     * @Route("/index.php")
     */
    /*
    .
    */
    public function loginCheckAction()
    {
     return $this->redirectToRoute("homepage");
    }


 

    /**
     * @Route("/profesor/api/listaAlumnos")
     * @Method("GET")
     */
  public function listarAlumnosJsonAction(){

    $lista= Profesor::getListaAlumnos();

     $data =["array"=>$lista];

     return new JsonResponse($data);
  }
   
}



